#!/bin/env python3
"""Scanner.

Usage:
  scanner.py (-i | --input) <input_file> (-o | --output) <log_file> (-c | --cert) <cert_file> (-b | --block) <block_list_file> [--no-crash] [--test-run] 
  scanner.py (-h | --help)
  scanner.py --version

Options:
  -h --help     Show this screen.
  --version     Show version.
  -i --input    List of to scan domains/IP addresses.
  -o --output   Dumps all connection information used in analysis to log files.
  -c --cert     Validate certificate chains on the fly.
  -b --block    Takes a block list of IPs and domain names.
  --test-run    Limit to only 5 random IPs
  --nocrash     On errors and exceptions do not exit the program.

"""
import ipaddress
import json
from ssl import PROTOCOL_TLS, VerifyMode

from dataclasses import dataclass
from dataclasses_json import dataclass_json
from urllib.request import ssl, socket
from random import randint
from xmlrpc.client import boolean
from docopt import docopt

PORT = '443'
TIMEOUT = 2 # timeout after 2 seconds

@dataclass_json
@dataclass
class TLSLog:
    TLSVER: int
    VALID: boolean
    CA: str
    CTLOG: str

# Reads the block list and returns a tuple
# containing two arrays containing blocked IP's
# and blocked domains
def read_block_list(block_list_file):
    blocked_ips = []
    blocked_domains = []

    with open(block_list_file) as file:
        for line in file:
            s_line = line.rstrip()
            try:
                ip = ipaddress.ip_network(s_line)
            except ValueError:
                # print("domain: ", s_line)
                blocked_domains += [s_line]
            else:
                # print("IP: ", ip)
                blocked_ips += [ip]
    return (blocked_ips, blocked_domains)

def scan_targets(input_file, blocked_ips, blocked_domains, no_crash, cert_file, test_run):
    if (test_run):
        value = randint(0, 1000)
        acc_i = range(value, value+20)
    
    ssl_context = ssl.SSLContext(PROTOCOL_TLS)
    ssl_context.verify_mode = VerifyMode.CERT_REQUIRED
    ssl_context.load_verify_locations(cert_file)

    i = 0
    with open(input_file) as input:
        tls_logs = {}
        for line in input:
            
            if(test_run):
                if (i not in acc_i):
                    i = i + 1
                    continue
                
            i = i + 1

            row = line.rstrip().split(',')
            domain = row[0]
            ip = ipaddress.ip_network(row[1])

            # print(f'i: {i}, {row[0]} with IP {row[1]}.')

            if ip in blocked_ips:
                print(f'Skipping {row[0]} with IP {row[1]} because it matches a blocked IP.')
                continue
            if domain in blocked_domains: # the domain has to be an exact match with one in block list 
                print(f'Skipping {row[0]} with IP {row[1]} because it matches a blocked domain.')
                continue
            
            try: 
                sock = socket.create_connection((domain, PORT), timeout=TIMEOUT)
            except Exception as ex:
                print(f'{ex} on {row[0]} with IP {row[1]}.')
                if (not no_crash):
                        exit(1)
                continue
            
            ssock = ssl_context.wrap_socket(sock, server_hostname=domain, do_handshake_on_connect=False)
            try:
                ssock.do_handshake()
                tls_ver = ssock.version()
                ca = ssock.getpeercert()['issuer'][1][0][1]
                ctlog=ssock.getpeercert()['OCSP'][0]
                tls_logs[domain] = TLSLog(TLSVER=tls_ver, VALID=True, CA=ca, CTLOG=ctlog)
            except ssl.SSLCertVerificationError as ex:
                print(f'For {row[0]} with IP {row[1]} the following exception occured {ex}.')
                tls_ver = ssock.version()
                tls_logs[domain] = TLSLog(TLSVER=tls_ver, VALID=False, CA='', CTLOG='')
                if (not no_crash):
                        exit(1)
            except Exception as ex:
                print(f'For {row[0]} with IP {row[1]} the following exception occured {ex}.')
                if (not no_crash):
                    exit(1)

    return tls_logs
                
def save_results(results, log_file):
    json_results = {}
    for key, value in results.items():
        json_results[key] = value.__dict__

    f = open(log_file, "w")
    f.write(json.dumps(json_results, indent=4))
    f.close()

if __name__ == '__main__':
    arguments = docopt(__doc__, version='ESA TLS Scanner version 0.1')
    # print(arguments)
    (blocked_ips, blocked_domains) = read_block_list(arguments['<block_list_file>'])
    # print('Blocked IP\'s: ', blocked_ips)
    # print('Blocked domains: ', blocked_domains)
    results = scan_targets(arguments['<input_file>'], blocked_ips, blocked_domains, arguments['--no-crash'], arguments['<cert_file>'], arguments['--test-run'])
    save_results(results, arguments['<log_file>'])



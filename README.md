<br />
<div align="center">
  <a href="">
    <h3 align="center">ESA Assignment 2</h3>
  </a>

  <p align="center">
    TLS scanner and analyser
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#roadmap">Roadmap</a></li>
  </ol>
</details>

## About The Project

In this assignment, we wrote a TLS/X.509 scanner from scratch. With in addition a script that analyses the found records.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## Getting Started

### Prerequisites

A few programs are needed before the installation and use of the python scripts.
* pip
  ```sh
  sudo apt install -y python3-pip
  ```

### Installation

1. Clone the repo
   ```sh
   git clone 
   ```
2. Install all required libraries
  ```sh
  python3 -m pi3p install --user -r requirements.txt
  ```
### Run Program
For more information run the program with the help flag (-h or --help).
1. Running the scanner
   ```sh
   ./scanner.py  -i <input_file> -o <log_file> -c <root_certs.pem> -b <block_list> --no-crash 
   ```
2. Running the analysis code
  ```sh
  ./analysis.py -i <log_file>
  ```
<p align="right">(<a href="#readme-top">back to top</a>)</p>


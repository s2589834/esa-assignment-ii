#!/bin/env python3
"""Analysis.

Usage:
  analysis.py (-i | --input) <input_file>
  analysis.py (-h | --help)
  analysis.py --version

Options:
  -h --help     Show this screen.
  --version     Show version.
  -i --input    JSON input file containing results from scan
"""
import json
from docopt import docopt

def read_json(json_input_file):
    with open(json_input_file, 'r') as f:
        data = json.load(f)
    
    return data

def get_tls_version_percentage(data):
    print("==== TLS versions ====")
    tls_versions = []
    for key, value in data.items():
        tls_versions += [str(value['TLSVER'])]

    for tls_version in sorted(set(tls_versions)):
        tls_v_perc = len([x for x in tls_versions if x == tls_version]) / len(tls_versions) * 100
        print(f'{tls_version}: is used in {tls_v_perc:.2f}% of the domains')

def get_percentage_valid(data):
    print("\n==== Valid Certificates ====")
    perc_valid = len([x for x in data.values() if x['VALID'] == True]) / len(data.values()) * 100
    print(f'In {perc_valid:.2f}% of the records the given certificate has been valid')

def get_ca_percentage(data):
    print("\n==== Certificate Authorities ====")
    cas = []
    for key, value in data.items():
        cas += [value['CA']]

    for ca in sorted(set(cas)):
        if ca != '':
            ca_perc = len([x for x in cas if x == ca]) / len([x for x in cas if x != '']) * 100
            print(f'{ca_perc:.2f}% of domains with valid cert chain use CA {ca}')

def get_ct_percentage(data):
    print("\n==== Certificate Transparency ====")
    ctlogs = []
    for key, value in data.items():
        ctlogs += [str(value['CTLOG'])]

    for ctlog in sorted(set(ctlogs)):
        if ctlog != '':
            ca_perc = len([x for x in ctlogs if x == ctlog]) / len([x for x in ctlogs if x != '']) * 100
            print(f'{ca_perc:.2f}% of domains use log {ctlog}')

if __name__ == '__main__':
    arguments = docopt(__doc__, version='ESA TLS Analysis version 0.1')
    data = read_json(arguments['<input_file>'])
    get_tls_version_percentage(data)
    get_percentage_valid(data)
    get_ca_percentage(data)
    get_ct_percentage(data)
